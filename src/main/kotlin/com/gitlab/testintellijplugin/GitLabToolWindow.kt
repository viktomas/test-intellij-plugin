package com.gitlab.testintellijplugin

import com.gitlab.testintellijplugin.services.GitLabApiService
import com.gitlab.testintellijplugin.services.GitService
import com.gitlab.testintellijplugin.services.RepositoryChangeListener
import com.intellij.icons.AllIcons
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.ui.components.Panel
import com.intellij.ui.layout.panel
import com.intellij.ui.treeStructure.Tree
import kotlinx.coroutines.runBlocking
import javax.swing.tree.DefaultMutableTreeNode
import javax.swing.tree.DefaultTreeCellRenderer


class GitLabToolWindow(private val project: Project, private val toolWindow: ToolWindow) {

    val panel = Panel();
   init{
       val gitService = project.service<GitService>()
       gitService.addListener(object: RepositoryChangeListener {
           override fun onChange() {
               // TODO, I probably don't need to do the whole thing on the event dispatch thread
               val application = ApplicationManager.getApplication()
               if (application.isDispatchThread) refresh()
               else application.invokeLater({ refresh() }) { project.isDisposed }
           }
       })
       refresh();
   }

    fun refresh() {
        val gitLabService = project.service<GitLabApiService>()
        val gitService = project.service<GitService>()
        val contentManager = toolWindow.contentManager
        val content = contentManager.factory.createContent(panel, "", true)
        contentManager.addContent(content)
        if(gitService.getRepositories().isEmpty()){
            val messageLabel = panel(){
                noteRow("There are no open repositories")
            }
            panel.removeAll();
            panel.add(messageLabel);
        } else {
            runBlocking {
                val gitRemote = gitService.getRemote();
                val glProject = gitLabService.getGitLabProject(gitRemote.namespace, gitRemote.project)
                val top = DefaultMutableTreeNode(glProject.name)
                top.add(DefaultMutableTreeNode("Issues"))
                val mrRootItem = DefaultMutableTreeNode("MRs")
                val mrs = gitLabService.getMRs(glProject)
                val mrItems = mrs.map{ DefaultMutableTreeNode(it.title) }
                mrItems.forEach{mrRootItem.add(it)}
                top.add(mrRootItem)
                val tree = Tree(top)
                val renderer = DefaultTreeCellRenderer()
                renderer.leafIcon = AllIcons.Actions.ProjectDirectory
                tree.cellRenderer = renderer;
                panel.removeAll();
                panel.add(tree);
            }
        }
    }
}