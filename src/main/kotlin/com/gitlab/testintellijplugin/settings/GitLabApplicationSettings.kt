package com.gitlab.testintellijplugin.settings

import com.intellij.ide.BrowserUtil
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.layout.panel

class GitLabApplicationSettings: BoundConfigurable("GitLab IntelliJ Plugin") {
    fun openPopup(): Unit {
        TokenDialog().showAndGet()
    }

    override fun createPanel(): DialogPanel {
        return panel {
            row {
                cell {
                    label("Configure Access Tokens")
                }
                cell {
                    button("Configure GitLab Access Tokens"){openPopup()}
                }
            }
            noteRow("""Links work here <a href="https://gitlab.com">GitLab</a>""") {
                BrowserUtil.browse(it)
            }
        }
    }
}