package com.gitlab.testintellijplugin.settings

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.diagnostic.logger


@Service(Service.Level.PROJECT)
@State(name = "ProjectSettingsData", storages = [Storage("settingsPanelData.xml")])
class ProjectSettingsData : PersistentStateComponent<ProjectSettingsData.State> {

    private val LOG = logger<ProjectSettingsData>()

    var myState = State()

    override fun getState(): State {

        return myState
    }

    override fun loadState(stateLoadedFromPersistence: State) {
        LOG.info("ProjectSettingsData.loadState")
        myState = stateLoadedFromPersistence
    }

    data class State(var instanceUrl: String = "https://gitlab.com") {
        override fun toString(): String =
            "State{ instanceUrl: '$instanceUrl' }"
    }
}