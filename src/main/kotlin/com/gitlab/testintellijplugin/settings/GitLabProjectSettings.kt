package com.gitlab.testintellijplugin.settings

import com.intellij.ide.BrowserUtil
import com.intellij.openapi.components.service
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.layout.panel

class GitLabProjectSettings(val project: Project) : BoundConfigurable("GitLab IntelliJ Plugin") {
    override fun createPanel(): DialogPanel {
        val projectSettingsData = project.service<ProjectSettingsData>()
        return panel {
            row {
                cell {
                    label("Instance URL")
                }
                cell {
                    textField(projectSettingsData.myState::instanceUrl)
                }
            }
            noteRow("""Links work here <a href="https://gitlab.com">GitLab</a>""") {
                BrowserUtil.browse(it)
            }
        }
    }
}