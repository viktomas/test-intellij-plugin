package com.gitlab.testintellijplugin.settings

import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.layout.panel
import javax.swing.JComponent

class NewTokenDialog : DialogWrapper(true) {
    var instanceUrl = ""
    var token = ""

    init {
        title = "Add/Edit Token"
        init()
    }

    override fun createCenterPanel(): JComponent {
        return panel {
            row {
                label("GitLab instance URL")
                textField(::instanceUrl)
            }
            row {
                label("Token")
                textField(::token)
            }
        }
    }
}
