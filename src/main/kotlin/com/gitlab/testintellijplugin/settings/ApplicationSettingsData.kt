package com.gitlab.testintellijplugin.settings

import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.generateServiceName
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.diagnostic.logger


@Service
@State(name = "ApplicationSettingsData", storages = [Storage("gitlab-application-settings.xml")])
class ApplicationSettingsData : PersistentStateComponent<ApplicationSettingsData.State> {

    private val LOG = logger<ApplicationSettingsData>()

    companion object {
        val instance: ApplicationSettingsData
            get() = ApplicationManager.getApplication().getService(ApplicationSettingsData::class.java)
    }

    var myState = State()

    private fun createCredentialAttributes(key: String): CredentialAttributes {
        return CredentialAttributes(
            generateServiceName("GitLab Intellij Plugin", key)
        )
    }

    fun getPassword(key: String): String?{
        val credentialAttributes = createCredentialAttributes(key)

        return PasswordSafe.instance.getPassword(credentialAttributes)
    }

    fun removePassword(key: String) {
        val credentialAttributes = createCredentialAttributes(key) // see previous sample
        PasswordSafe.instance.setPassword(credentialAttributes, null)
    }

    fun setPassword(key: String, password: String) {
        val credentialAttributes = createCredentialAttributes(key) // see previous sample

        PasswordSafe.instance.setPassword(credentialAttributes, password)
    }

    override fun getState(): State {

        return myState
    }

    override fun loadState(stateLoadedFromPersistence: State) {
        LOG.info("ApplicationSettingsData.loadState")
        myState = stateLoadedFromPersistence
    }

    class State {
        var instancesWithToken: Set<String> = HashSet()

        override fun toString(): String =
            "State{ instancesWithToken: '$instancesWithToken' }"
    }
}