package com.gitlab.testintellijplugin.settings

import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.layout.panel
import javax.swing.JComponent
import javax.swing.JPanel


class TokenDialog : DialogWrapper(true) {
    val rootPanel = JPanel()
    init {
        title = "Configure GitLab Access Tokens"
        init()
    }

    fun deleteToken(instanceUrl: String) {
        val instancesWithToken = ApplicationSettingsData.instance.myState.instancesWithToken.toMutableSet()
        instancesWithToken.remove(instanceUrl)
        ApplicationSettingsData.instance.removePassword(instanceUrl)
        ApplicationSettingsData.instance.myState.instancesWithToken = instancesWithToken
        refresh()
    }

    fun addToken() {
        val addDialog = NewTokenDialog()
        if(addDialog.showAndGet()){
            val instancesWithToken = ApplicationSettingsData.instance.myState.instancesWithToken.toMutableSet()
            instancesWithToken.add(addDialog.instanceUrl)
            ApplicationSettingsData.instance.setPassword(addDialog.instanceUrl, addDialog.token)
            ApplicationSettingsData.instance.myState.instancesWithToken = instancesWithToken
            refresh()
        }
    }

    fun maskToken(token: String): String{
        if (token.length < 8 ) return "********"
        return "**********${token.takeLast(4)}"
    }

    fun refresh() {
        val table = panel {
            row {
                label("GitLab Instance URL")
                label("Personal Access Token")
            }
            ApplicationSettingsData.instance.myState.instancesWithToken.forEach {
                val instanceUrl = it
                row {
                    label(instanceUrl)
                    label(maskToken(ApplicationSettingsData.instance.getPassword(instanceUrl)!!))
                    button("Edit") {}
                    button("Delete") { deleteToken(instanceUrl)}
                }
            }
            row {
                button("Add token"){ addToken()}
            }
        }

        rootPanel.removeAll()
        rootPanel.add(table)
        rootPanel.revalidate()

    }

    override fun createCenterPanel(): JComponent {
        refresh()
        return rootPanel
    }
}