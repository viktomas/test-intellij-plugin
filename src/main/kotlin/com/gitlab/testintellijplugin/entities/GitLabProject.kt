package com.gitlab.testintellijplugin.entities

import kotlinx.serialization.*

// https://github.com/Kotlin/kotlinx.serialization/issues/993#issuecomment-984742051
@Suppress("PROVIDED_RUNTIME_TOO_LOW")
@Serializable
data class GitLabProject(val id: Int, val name: String, val description: String)