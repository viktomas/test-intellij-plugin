package com.gitlab.testintellijplugin.entities

import kotlinx.serialization.Serializable

// https://github.com/Kotlin/kotlinx.serialization/issues/993#issuecomment-984742051
@Suppress("PROVIDED_RUNTIME_TOO_LOW")
@Serializable
data class GitLabMr(val id: Int, val iid: Int, val title: String, val description: String)
