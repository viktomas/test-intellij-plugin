package com.gitlab.testintellijplugin.services

interface RepositoryChangeListener {
    fun onChange()
}