package com.gitlab.testintellijplugin.services

import com.gitlab.testintellijplugin.MyBundle

class MyApplicationService {

    init {
        println(MyBundle.message("applicationService"))
    }
}
