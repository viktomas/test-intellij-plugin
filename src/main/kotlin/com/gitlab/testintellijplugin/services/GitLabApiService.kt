package com.gitlab.testintellijplugin.services

import com.intellij.openapi.project.Project
import com.gitlab.testintellijplugin.MyBundle
import com.gitlab.testintellijplugin.entities.GitLabMr
import com.gitlab.testintellijplugin.entities.GitLabProject
import com.gitlab.testintellijplugin.settings.ApplicationSettingsData
import com.gitlab.testintellijplugin.settings.ProjectSettingsData
import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.extensions.PluginId
import io.ktor.client.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.client.features.json.serializer.*

class GitLabApiService(private val project: Project) {
    private val LOG: Logger = Logger.getInstance(GitLabApiService::class.java)
    private val client = HttpClient(){
        install(JsonFeature){
            serializer = KotlinxSerializer(kotlinx.serialization.json.Json{
                isLenient = true
                ignoreUnknownKeys = true
            })
        }
        defaultRequest {
            header("User-Agent", getUserAgent())
        }
    }

    init {
        LOG.info("Loaded test project")
        println(MyBundle.message("projectService", project.name))
    }

    suspend fun getGitLabProject(namespace: String, projectName: String): GitLabProject {
        val fullProjectName = "$namespace%2F$projectName"
        val instanceUrl = project.service<ProjectSettingsData>().myState.instanceUrl
        val token = ApplicationSettingsData.instance.getPassword(instanceUrl)
        if (token === null) throw Error("no token for $instanceUrl")
        return client.get("$instanceUrl/api/v4/projects/$fullProjectName"){
            header("Authorization", "Bearer: $token")
        }
    }

    suspend fun getMRs(glProject: GitLabProject): List<GitLabMr>{
        val instanceUrl = project.service<ProjectSettingsData>().myState.instanceUrl
        val token = ApplicationSettingsData.instance.getPassword(instanceUrl)
        if (token === null) throw Error("no token for $instanceUrl")
        return client.get("$instanceUrl/api/v4/projects/${glProject.id}/merge_requests"){
            header("Authorization", "Bearer: $token")
        }
    }

    companion object {

        fun sanitize(s: String): String {
           return s.lowercase().replace(Regex("\\s"), "-")
        }
        fun getUserAgent(): String {
            val pluginVersion = PluginManagerCore.getPlugin(PluginId.getId("com.github.viktomasgitlab.testintellijplugin"))?.version
//            val applicationNameAndVersion = ApplicationInfo.getInstance().fullApplicationName
            val applicationName = ApplicationInfo.getInstance().versionName
            val applicationVersion = ApplicationInfo.getInstance().fullVersion
            val intellijPlatformInfo = sanitize("$applicationName/$applicationVersion")
            val javaVersion = System.getProperty("java.version")
            val os = sanitize("${System.getProperty("os.name")}/${System.getProperty("os.arch")}/${System.getProperty("os.version")}")
            val javaInfo = sanitize("java/$javaVersion")
            return "intellij-gitlab-plugin/$pluginVersion $intellijPlatformInfo $javaInfo $os"
        }
    }
}
