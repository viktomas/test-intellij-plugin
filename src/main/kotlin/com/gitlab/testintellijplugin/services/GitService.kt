package com.gitlab.testintellijplugin.services

import com.gitlab.testintellijplugin.git.GitRemote
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryChangeListener
import git4idea.repo.GitRepositoryManager

@Service(Service.Level.PROJECT)
class GitService(private val project: Project) {

    private val repoListeners = ArrayList<RepositoryChangeListener>()

    fun getRepositories(): List<GitRepository> {
        return GitRepositoryManager.getInstance(project).repositories
    }

    fun getRemote():GitRemote {
        val repo = GitRepositoryManager.getInstance(project).repositories.first()
        val remote = repo.remotes.first()
        val url = remote.firstUrl ?: throw Exception("No remote url");
        return GitRemote.parseRemote(url) ?: throw Exception("Failed to parse git remote $url")
    }

    fun addListener(listener: RepositoryChangeListener) {
        repoListeners.add(listener)
    }

    fun repositoryUpdate() {
        repoListeners.forEach{it.onChange()}
    }


    class GitRepoChangeListener(private val proj: Project) : GitRepositoryChangeListener {
        override fun repositoryChanged(repository: GitRepository) {
            val service = proj.service<GitService>()
            service.repositoryUpdate()
        }

    }
}