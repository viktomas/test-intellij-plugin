package com.gitlab.testintellijplugin.listeners

import com.gitlab.testintellijplugin.services.GitLabApiService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManagerListener

internal class MyProjectManagerListener : ProjectManagerListener {

    override fun projectOpened(project: Project) {
        project.service<GitLabApiService>()
    }
}
