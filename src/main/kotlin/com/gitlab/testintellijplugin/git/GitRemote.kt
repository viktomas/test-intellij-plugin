package com.gitlab.testintellijplugin.git

data class GitRemote(val host: String, val namespace: String, val project: String) {
    companion object {
        // git@gitlab.com:gitlab-org/gitlab-vscode-extension.git
        private val REMOTE_REGEXP = """git@(.+):(.+)/([^/]+).git""".toRegex()

        fun parseRemote(gitRemoteUrl: String): GitRemote? {
            val regexGroups = REMOTE_REGEXP.matchEntire(gitRemoteUrl)?.groups ?: return null
            val host = regexGroups[1]?.value
            val namespace = regexGroups[2]?.value
            val project = regexGroups[3]?.value
            return GitRemote(host!!,namespace!!,project!!)
        }
    }
}
