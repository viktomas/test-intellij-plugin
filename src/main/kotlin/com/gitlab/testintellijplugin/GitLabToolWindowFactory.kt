package com.gitlab.testintellijplugin

import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory


class GitLabToolWindowFactory: ToolWindowFactory {
    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val glToolWindow = GitLabToolWindow(project,toolWindow);
        val contentManager = toolWindow.contentManager
        val content = contentManager.factory.createContent(glToolWindow.panel, "", true)
        contentManager.addContent(content)
    }
}