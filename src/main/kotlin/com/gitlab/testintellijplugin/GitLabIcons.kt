package com.gitlab.testintellijplugin

import com.intellij.openapi.util.IconLoader

object GitLabIcons {
    @JvmField
    val Tanuki  = IconLoader.getIcon("/icons/tanuki.svg", javaClass)
}