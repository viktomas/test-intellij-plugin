package com.gitlab.testintellijplugin.git

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.Assertions.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GitRemoteTest {
    val remoteUrl = "git@gitlab.com:gitlab-org/gitlab-vscode-extension.git"

    @Test
    fun `test parsing remote`() {
       val parsedRemote = GitRemote.parseRemote(remoteUrl)

        assertEquals(parsedRemote, GitRemote("gitlab.com", "gitlab-org", "gitlab-vscode-extension"))
    }
}